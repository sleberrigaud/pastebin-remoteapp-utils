package org.leberrigaud.pastebin.utils;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.leberrigaud.pastebin.utils.HashUtils.*;


public final class HashUtilsTest
{
    @Test
    public void testSha1() throws Exception
    {
        assertEquals("bd688cd2be86e90e267f76cba1c2fd00ba57bf96", sha1("this_is_my_test_string"));
    }
}
