package org.leberrigaud.pastebin.utils;

import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

final class HashUtils
{
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    public static String sha1(String content)
    {
        return Hashing.sha1().hashBytes(content.getBytes(UTF_8)).toString();
    }
}
