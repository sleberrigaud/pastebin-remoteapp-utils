package org.leberrigaud.pastebin.utils;

public final class PastebinUrl
{
    private PastebinUrl()
    {
    }

    public static String id(String url)
    {
        if (isUrl(url))
        {
            return url.substring(url.lastIndexOf('/') + 1);
        }
        return url;
    }

    public static boolean isUrl(String url)
    {
        return url.startsWith("http://") || url.startsWith("https://");
    }
}
