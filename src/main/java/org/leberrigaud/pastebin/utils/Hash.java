package org.leberrigaud.pastebin.utils;

import com.google.common.base.Joiner;

public final class Hash
{
    private Hash()
    {
    }

    public static String of(String body, String lang)
    {
        return HashUtils.sha1(Joiner.on('~').join(lang, body));
    }
}
