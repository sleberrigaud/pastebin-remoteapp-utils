package org.leberrigaud.pastebin.utils;

import com.atlassian.fugue.Option;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;

public final class Api
{
    public static <R> R post(String body, String lang, Call<R> call, Supplier<Option<String>> pasteBinUserKey)
    {
        final ImmutableMap.Builder<String, String> parameters = ImmutableMap.<String, String>builder()
                .put("api_dev_key", "ab417b3208c08568fe3886c52bfd2b94")
                .put("api_option", "paste")
                .put("api_paste_format", lang)
                .put("api_paste_code", body);

        final Option<String> pastebinUserKey = pasteBinUserKey.get();
        if (pastebinUserKey.isDefined())
        {
            parameters.put("api_user_key", pastebinUserKey.get());
        }

        return call.call("http://pastebin.com/api/api_post.php", parameters.build());
    }

    public static interface Call<R>
    {
        R call(String url, ImmutableMap<String, String> parameters);
    }
}
